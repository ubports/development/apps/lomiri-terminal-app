# Persian translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 12:32+0000\n"
"PO-Revision-Date: 2022-03-10 09:21+0000\n"
"Last-Translator: Behzad <behzadasbahi@gmail.com>\n"
"Language-Team: Persian <https://translate.ubports.com/projects/ubports/"
"terminal-app/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-03-04 06:22+0000\n"

#: lomiri-terminal-app.desktop.in:7
msgid "/usr/share/lomiri-terminal-app/terminal-app.svg"
msgstr ""

#: lomiri-terminal-app.desktop.in:8 app/qml/TerminalPage.qml:72
msgid "Terminal"
msgstr "پایانه"

#: app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "گزینش"

#: app/qml/AlternateActionPopover.qml:73
#: app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "رونوشت"

#: app/qml/AlternateActionPopover.qml:79
#: app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "جاگذاری"

#: app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr ""

#: app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr ""

#: app/qml/AlternateActionPopover.qml:98
msgid "Close active view"
msgstr ""

#: app/qml/AlternateActionPopover.qml:105
#: app/qml/Settings/SettingsShortcutsSection.qml:151 app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "زبانه‌ی جدید"

#: app/qml/AlternateActionPopover.qml:110
#, fuzzy
msgid "Close active tab"
msgstr "پایانه"

#: app/qml/AlternateActionPopover.qml:120
#: app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr ""

#: app/qml/AlternateActionPopover.qml:127 app/qml/AuthenticationDialog.qml:70
#: app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "لغو"

#: app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "تأیید هویت لازم است"

#: app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "رمزعبور یا عبارت‌عبور را وارد کنید"

#: app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "رمزعبور یا عبارت‌عبور"

#: app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "تصدیق کردن"

#: app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "تأیید هویت شکست خورد"

#: app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr ""

#: app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""

#: app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr ""

#: app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "تعویض صفحه‌کلید"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr "کلیدهای واپایش"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr "کلیدهای تابع"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "کلیدهای لغزش"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr "کلیدهای فرمان"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr ""

#. TRANSLATORS: This is the name of the Control key.
#: app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "مهار"

#. TRANSLATORS: This is the name of the Alt key.
#: app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr ""

#. TRANSLATORS: This is the name of the Shift key.
#: app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr ""

#. TRANSLATORS: This is the name of the Escape key.
#: app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr ""

#. TRANSLATORS: This is the name of the Page Up key.
#: app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr ""

#. TRANSLATORS: This is the name of the Page Down key.
#: app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr ""

#. TRANSLATORS: This is the name of the Delete key.
#: app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr ""

#. TRANSLATORS: This is the name of the Home key.
#: app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "خانه"

#. TRANSLATORS: This is the name of the End key.
#: app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr ""

#. TRANSLATORS: This is the name of the Tab key.
#: app/qml/KeyboardRows/JsonTranslator.qml:115
#, fuzzy
msgid "Tab"
msgstr "زبانه‌ها"

#. TRANSLATORS: This is the name of the Enter key.
#: app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr ""

#: app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "قبول"

#: app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:66
#, fuzzy
msgid "Font:"
msgstr "اندازه‌ی قلم:"

#: app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "اندازه‌ی قلم:"

#: app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "اوبونتو"

#: app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "سبز روی سیاه"

#: app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "سفید روی سیاه"

#: app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "سیاه روی سفید"

#: app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "شیاه روی روشن تصادفی"

#: app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "لینوکس"

#: app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "پایانهٔ قدیمی باحال"

#: app/qml/Settings/SettingsInterfaceSection.qml:131
#, fuzzy
msgid "Dark pastels"
msgstr "مداد شمعی تیره / اوبونتو (قدیمی)"

#: app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "سیاه روی زرد روشن"

#: app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "چینش‌ها"

#: app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:145
#: app/qml/Settings/SettingsShortcutsSection.qml:150
#: app/qml/Settings/SettingsShortcutsSection.qml:155
#: app/qml/Settings/SettingsShortcutsSection.qml:160
#: app/qml/Settings/SettingsShortcutsSection.qml:165
#: app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:156
#, fuzzy
msgid "Close terminal"
msgstr "پایانه"

#: app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:171
#, fuzzy
msgid "Next tab"
msgstr "زبانه‌ی جدید"

#: app/qml/Settings/SettingsShortcutsSection.qml:175
#: app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "ویرایش"

#: app/qml/Settings/SettingsShortcutsSection.qml:185
#: app/qml/Settings/SettingsShortcutsSection.qml:190
#: app/qml/Settings/SettingsShortcutsSection.qml:195
#: app/qml/Settings/SettingsShortcutsSection.qml:200
#: app/qml/Settings/SettingsShortcutsSection.qml:205
#: app/qml/Settings/SettingsShortcutsSection.qml:210
#: app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr ""

#: app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr ""

#: app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr ""

#: app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr ""

#: app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "زبانه‌ها"

#: app/qml/TerminalPage.qml:326
#, fuzzy
#| msgid "Selection Mode"
msgid "exit selection mode"
msgstr "حالت گزینش"

#~ msgid "Un-named Color Scheme"
#~ msgstr "طرح رنگ بی‌نام"

#~ msgid "Accessible Color Scheme"
#~ msgstr "طرح رنگ دست‌رس پذیر"

#~ msgid "Open Link"
#~ msgstr "گشودن پیوند"

#~ msgid "Copy Link Address"
#~ msgstr "رونوشت از نشانی پیوند"

#, fuzzy
#~ msgid "Send Email To…"
#~ msgstr "ارسال ای‌میل به…"

#~ msgid "Copy Email Address"
#~ msgstr "رونوشت برداشت از نشانی ای‌میل"

#~ msgid "Match case"
#~ msgstr "تطابق کوچک و بزرگی حروف"

#~ msgid "Regular expression"
#~ msgstr "عبارت باقاعده"

#~ msgid "Higlight all matches"
#~ msgstr "روشن کردن تمام موارد مطابق"

#~ msgid ""
#~ "No keyboard translator available.  The information needed to convert key "
#~ "presses into characters to send to the terminal is missing."
#~ msgstr ""
#~ "هیچ مترجم صفحه‌کلیدی موجود نیست. اطَلاعات مورد نیاز برای تبدیل کلیدهای "
#~ "فشرده شده به نویسه‌ها برای ارسال به پایانه وجود ندارند."

#~ msgid "Color Scheme Error"
#~ msgstr "خطای شمای رنگ"

#, qt-format
#~ msgid "Cannot load color scheme: %1"
#~ msgstr "نمی‌توان شمای رنگ را بار کرد: %1"

#~ msgid "Color Scheme"
#~ msgstr "شمای رنگ"

#~ msgid "FNS"
#~ msgstr "تابع"

#~ msgid "SCR"
#~ msgstr "لغزش"

#~ msgid "CMD"
#~ msgstr "فرمان"

#~ msgid "ALT"
#~ msgstr "دگرساز"

#~ msgid "SHIFT"
#~ msgstr "تبدیل"

#~ msgid "ESC"
#~ msgstr "فرار"

#~ msgid "PG_UP"
#~ msgstr "صفحه بالا"

#~ msgid "PG_DN"
#~ msgstr "صفحه پایین"

#~ msgid "DEL"
#~ msgstr "حذف"

#~ msgid "HOME"
#~ msgstr "خانه"

#~ msgid "END"
#~ msgstr "انتها"

#~ msgid "TAB"
#~ msgstr "جهش"

#~ msgid "ENTER"
#~ msgstr "ورود"

#~ msgid "Ok"
#~ msgstr "تأیید"

#~ msgid "Settings"
#~ msgstr "تنظیمات"

#~ msgid "Show Keyboard Bar"
#~ msgstr "نمایش نوار صفحه‌کلید"

#~ msgid "Show Keyboard Button"
#~ msgstr "نمایش دکمهٔ صفحه‌کلید"

#~ msgid "Enter password:"
#~ msgstr "گذرواژه را وارد کنید:"

#~ msgid "password"
#~ msgstr "گذرواژه"
