# Belarusian translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 12:32+0000\n"
"PO-Revision-Date: 2020-11-27 09:26+0000\n"
"Last-Translator: Zmicer <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"terminal-app/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"

#: lomiri-terminal-app.desktop.in:7
msgid "/usr/share/lomiri-terminal-app/terminal-app.svg"
msgstr ""

#: lomiri-terminal-app.desktop.in:8 app/qml/TerminalPage.qml:72
msgid "Terminal"
msgstr "Тэрмінал"

#: app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Абраць"

#: app/qml/AlternateActionPopover.qml:73
#: app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Скапіяваць"

#: app/qml/AlternateActionPopover.qml:79
#: app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Уставіць"

#: app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr "Падзяліць гарызантальна"

#: app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr "Падзяліць вертыкальна"

#: app/qml/AlternateActionPopover.qml:98
msgid "Close active view"
msgstr ""

#: app/qml/AlternateActionPopover.qml:105
#: app/qml/Settings/SettingsShortcutsSection.qml:151 app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Новая ўкладка"

#: app/qml/AlternateActionPopover.qml:110
#, fuzzy
#| msgid "Close terminal"
msgid "Close active tab"
msgstr "Закрыць тэрмінал"

#: app/qml/AlternateActionPopover.qml:120
#: app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr "Новае акно"

#: app/qml/AlternateActionPopover.qml:127 app/qml/AuthenticationDialog.qml:70
#: app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Скасаваць"

#: app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Патрабуецца аўтэнтыфікацыя"

#: app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Увядзіце код доступу або парольную фразу:"

#: app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "код доступу або парольная фраза"

#: app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "Аўтэнтыфікацыя"

#: app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Памылка аўтэнтыфікацыі"

#: app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr "Няма запушчаных SSH-сервераў."

#: app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr "SSH-сервер не знойдзены. Хочаце працягнуць працу ў абмежаваным рэжыме?"

#: app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr "Працягнуць"

#: app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Змяніць клавіятуру"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr "Клавішы Ctrl"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr "Клавішы Fn"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "Клавішы стрэлак"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr "Клавішы Cmd"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr "Ctrl"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr "Fn"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr "Scr"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr "Cmd"

#. TRANSLATORS: This is the name of the Control key.
#: app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "CTRL"

#. TRANSLATORS: This is the name of the Alt key.
#: app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr "Alt"

#. TRANSLATORS: This is the name of the Shift key.
#: app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr "Shift"

#. TRANSLATORS: This is the name of the Escape key.
#: app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr "Esc"

#. TRANSLATORS: This is the name of the Page Up key.
#: app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr "PgUp"

#. TRANSLATORS: This is the name of the Page Down key.
#: app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr "PgDn"

#. TRANSLATORS: This is the name of the Delete key.
#: app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr "Del"

#. TRANSLATORS: This is the name of the Home key.
#: app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "Home"

#. TRANSLATORS: This is the name of the End key.
#: app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr "End"

#. TRANSLATORS: This is the name of the Tab key.
#: app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr "Tab"

#. TRANSLATORS: This is the name of the Enter key.
#: app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr "Enter"

#: app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "Добра"

#: app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr "Празрыстасць фону:"

#: app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr "R:"

#: app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr "G:"

#: app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr "B:"

#: app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr "Адрабіць"

#: app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr "Тэкст"

#: app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr "Памер шрыфту:"

#: app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Памер шрыфту:"

#: app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr "Колеры"

#: app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Зялёны на чорным"

#: app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Белы на чорным"

#: app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Чорны на белым"

#: app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Чорны на выпадковым светлым"

#: app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "Рэтра"

#: app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr "Цёмны фон"

#: app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "Чорны на светла-жоўтым"

#: app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr "Наладжана"

#: app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr "Фон:"

#: app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr "Тэкст:"

#: app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr "Звычайная палітра:"

#: app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr "Светлая палітра:"

#: app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr "Перадналады:"

#: app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Раскладкі"

#: app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr "закрыць"

#: app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr "Налады"

#: app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr "Інтэрфейс"

#: app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr "Спалучэнні клавіш"

#: app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr "Паказваецца %1 з %2"

#: app/qml/Settings/SettingsShortcutsSection.qml:145
#: app/qml/Settings/SettingsShortcutsSection.qml:150
#: app/qml/Settings/SettingsShortcutsSection.qml:155
#: app/qml/Settings/SettingsShortcutsSection.qml:160
#: app/qml/Settings/SettingsShortcutsSection.qml:165
#: app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr "Файл"

#: app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr "Закрыць тэрмінал"

#: app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr "Закрыць усе вокны тэрмінала"

#: app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr "Папярэдняя ўкладка"

#: app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr "Наступная ўкладка"

#: app/qml/Settings/SettingsShortcutsSection.qml:175
#: app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "Рэдагаваць"

#: app/qml/Settings/SettingsShortcutsSection.qml:185
#: app/qml/Settings/SettingsShortcutsSection.qml:190
#: app/qml/Settings/SettingsShortcutsSection.qml:195
#: app/qml/Settings/SettingsShortcutsSection.qml:200
#: app/qml/Settings/SettingsShortcutsSection.qml:205
#: app/qml/Settings/SettingsShortcutsSection.qml:210
#: app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr "Выгляд"

#: app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr "На ўвесь экран"

#: app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr "Падзяліць тэрмінал гарызантальна"

#: app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr "Падзяліць тэрмінал вертыкальна"

#: app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr "Перайсці да вышэйшага тэрмінала"

#: app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr "Перайсці да ніжэйшага тэрмінала"

#: app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr "Перайсці да тэрмінала злева"

#: app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr "Перайсці да тэрмінала справа"

#: app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr "Налады тэрмінала"

#: app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr "Увядзіце спалучэнне клавіш…"

#: app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr "Адключана"

#: app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Укладкі"

#: app/qml/TerminalPage.qml:326
#, fuzzy
#| msgid "Selection Mode"
msgid "exit selection mode"
msgstr "Рэжым вылучэння"

#~ msgid "Close App"
#~ msgstr "Закрыць праграму"

#~ msgid "Un-named Color Scheme"
#~ msgstr "Безназоўная схема колераў"

#~ msgid "Accessible Color Scheme"
#~ msgstr "Даступная схема колераў"

#~ msgid "Open Link"
#~ msgstr "Адкрыць спасылку"

#~ msgid "Copy Link Address"
#~ msgstr "Скапіяваць адрас спасылкі"

#~ msgid "Send Email To…"
#~ msgstr "Адправіць паведамленне…"

#~ msgid "Copy Email Address"
#~ msgstr "Скапіяваць адрас"

#~ msgid "Match case"
#~ msgstr "Зважаць на рэгістр"

#~ msgid "Regular expression"
#~ msgstr "Рэгулярны выраз"

#~ msgid "Higlight all matches"
#~ msgstr "Пазначыць усе супадзенні"

#~ msgid ""
#~ "No keyboard translator available.  The information needed to convert key "
#~ "presses into characters to send to the terminal is missing."
#~ msgstr ""
#~ "Не знойдзена табліцы адпаведнасці для клавіятуры. Адсутнічаюць звесткі, "
#~ "якія неабходныя для пераўтварэння сігналу ад клавіш у знакі, якія будуць "
#~ "паказвацца на экране."

#~ msgid "Color Scheme Error"
#~ msgstr "Памылка ў схеме колераў"

#, qt-format
#~ msgid "Cannot load color scheme: %1"
#~ msgstr "Не атрымалася загрузіць схему колераў: %1"

#~ msgid "Color Scheme"
#~ msgstr "Каляровая схема"

#~ msgid "FNS"
#~ msgstr "FNS"

#~ msgid "SCR"
#~ msgstr "SCR"

#~ msgid "CMD"
#~ msgstr "CMD"

#~ msgid "ALT"
#~ msgstr "ALT"

#~ msgid "SHIFT"
#~ msgstr "SHIFT"

#~ msgid "ESC"
#~ msgstr "ESC"

#~ msgid "PG_UP"
#~ msgstr "PG_UP"

#~ msgid "PG_DN"
#~ msgstr "PG_DN"

#~ msgid "DEL"
#~ msgstr "DEL"

#~ msgid "HOME"
#~ msgstr "HOME"

#~ msgid "END"
#~ msgstr "END"

#~ msgid "TAB"
#~ msgstr "TAB"

#~ msgid "ENTER"
#~ msgstr "ENTER"

#~ msgid "Ok"
#~ msgstr "ОК"

#~ msgid "Settings"
#~ msgstr "Налады"

#~ msgid "Show Keyboard Bar"
#~ msgstr "Паказаць клавіятуру"

#~ msgid "Enter password:"
#~ msgstr "Увядзіце пароль"

#~ msgid "password"
#~ msgstr "пароль"
