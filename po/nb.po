# Norwegian Bokmal translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 12:32+0000\n"
"PO-Revision-Date: 2021-03-15 05:27+0000\n"
"Last-Translator: Jan Christian Sherdahl <jan@sherdahl.no>\n"
"Language-Team: Norwegian Bokmål <https://translate.ubports.com/projects/"
"ubports/terminal-app/nb/>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"

#: lomiri-terminal-app.desktop.in:7
msgid "/usr/share/lomiri-terminal-app/terminal-app.svg"
msgstr ""

#: lomiri-terminal-app.desktop.in:8 app/qml/TerminalPage.qml:72
msgid "Terminal"
msgstr "Terminal"

#: app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Velg"

#: app/qml/AlternateActionPopover.qml:73
#: app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Kopier"

#: app/qml/AlternateActionPopover.qml:79
#: app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Lim inn"

#: app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr "Del vannrett"

#: app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr "Del loddrett"

#: app/qml/AlternateActionPopover.qml:98
msgid "Close active view"
msgstr ""

#: app/qml/AlternateActionPopover.qml:105
#: app/qml/Settings/SettingsShortcutsSection.qml:151 app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Ny fane"

#: app/qml/AlternateActionPopover.qml:110
#, fuzzy
#| msgid "Close terminal"
msgid "Close active tab"
msgstr "Lukk terminal"

#: app/qml/AlternateActionPopover.qml:120
#: app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr "Nytt vindu"

#: app/qml/AlternateActionPopover.qml:127 app/qml/AuthenticationDialog.qml:70
#: app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Avbryt"

#: app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Autentisering kreves"

#: app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Skriv inn passord:"

#: app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "passord"

#: app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "Autentiser"

#: app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Autentisering mislyktes"

#: app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr "Ingen SSH-tjener kjører."

#: app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr "Fant ikke SSH-tjeneren. Ønsker du å fortsette i avgrenset modus?"

#: app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr "Fortsett"

#: app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Endre tastatur"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr "Kontrolltaster"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr "Funksjonstaster"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "Rulletaster"

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr "Kommandotaster"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr "Ctrl"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr "Fn"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr "Scr"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr "Cmd"

#. TRANSLATORS: This is the name of the Control key.
#: app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "CTRL"

#. TRANSLATORS: This is the name of the Alt key.
#: app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr "Alt"

#. TRANSLATORS: This is the name of the Shift key.
#: app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr "Shift"

#. TRANSLATORS: This is the name of the Escape key.
#: app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr "Esc"

#. TRANSLATORS: This is the name of the Page Up key.
#: app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr "PgUp"

#. TRANSLATORS: This is the name of the Page Down key.
#: app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr "PgDn"

#. TRANSLATORS: This is the name of the Delete key.
#: app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr "Del"

#. TRANSLATORS: This is the name of the Home key.
#: app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "Home"

#. TRANSLATORS: This is the name of the End key.
#: app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr "End"

#. TRANSLATORS: This is the name of the Tab key.
#: app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr "Tab"

#. TRANSLATORS: This is the name of the Enter key.
#: app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr "Enter"

#: app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "OK"

#: app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr "Bakgrunnsdekkevne:"

#: app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr "R:"

#: app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr "G:"

#: app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr "B:"

#: app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr "Angre"

#: app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr "Tekst"

#: app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr "Skrifttype:"

#: app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Skriftstørrelse:"

#: app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr "Farger"

#: app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Grønt på svart"

#: app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Hvitt på sort"

#: app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Sort på hvitt"

#: app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Svart på tilfeldig lys"

#: app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "Kul retroterminal"

#: app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr "Mørke pastellfarger / Ubuntu (gammelt)"

#: app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "Sort på lysegult"

#: app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr "Egendefinert"

#: app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr "Bakgrunn:"

#: app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr "Tekst:"

#: app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr "Normal palett:"

#: app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr "Lys palett:"

#: app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr "Forvalg:"

#: app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Utforminger"

#: app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr "Lukk"

#: app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr "Innstillinger"

#: app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr "Grensesnitt"

#: app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr "Snarveier"

#: app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr "Viser %1 av %2"

#: app/qml/Settings/SettingsShortcutsSection.qml:145
#: app/qml/Settings/SettingsShortcutsSection.qml:150
#: app/qml/Settings/SettingsShortcutsSection.qml:155
#: app/qml/Settings/SettingsShortcutsSection.qml:160
#: app/qml/Settings/SettingsShortcutsSection.qml:165
#: app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr "Fil"

#: app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr "Lukk terminal"

#: app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr "Lukk alle terminaler"

#: app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr "Forrige fane"

#: app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr "Neste fane"

#: app/qml/Settings/SettingsShortcutsSection.qml:175
#: app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "Rediger"

#: app/qml/Settings/SettingsShortcutsSection.qml:185
#: app/qml/Settings/SettingsShortcutsSection.qml:190
#: app/qml/Settings/SettingsShortcutsSection.qml:195
#: app/qml/Settings/SettingsShortcutsSection.qml:200
#: app/qml/Settings/SettingsShortcutsSection.qml:205
#: app/qml/Settings/SettingsShortcutsSection.qml:210
#: app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr "Vis"

#: app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr "Veksle fullskjermsvisning"

#: app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr "Del terminal vannrett"

#: app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr "Del terminal loddrett"

#: app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr "Naviger til terminalen ovenfor"

#: app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr "Naviger til terminalen nedenfor"

#: app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr "Naviger til terminalen til venstre"

#: app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr "Naviger til terminalen til høyre"

#: app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr "Terminalinnstillinger"

#: app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr "Skriv inn snarvei…"

#: app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr "Deaktivert"

#: app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Faner"

#: app/qml/TerminalPage.qml:326
#, fuzzy
#| msgid "Selection Mode"
msgid "exit selection mode"
msgstr "Utvalgsmodus"

#~ msgid "Close App"
#~ msgstr "Lukk app"

#~ msgid "Un-named Color Scheme"
#~ msgstr "Fargeoppsett uten navn"

#~ msgid "Accessible Color Scheme"
#~ msgstr "Tilgjengelig fargeoppsett"

#~ msgid "Open Link"
#~ msgstr "Åpne lenke"

#~ msgid "Copy Link Address"
#~ msgstr "Kopier lenkeadresse"

#~ msgid "Send Email To…"
#~ msgstr "Send e-post til …"

#~ msgid "Copy Email Address"
#~ msgstr "Kopier e-postadresse"

#~ msgid "Match case"
#~ msgstr "Skill mellom store og små bokstaver"

#~ msgid "Regular expression"
#~ msgstr "Regulært uttrykk"

#~ msgid "Higlight all matches"
#~ msgstr "Uthev alle søkeresultater"

#~ msgid ""
#~ "No keyboard translator available.  The information needed to convert key "
#~ "presses into characters to send to the terminal is missing."
#~ msgstr ""
#~ "Ingen tasteoversetter er tilgjengelig. Mangler informasjon for å kunne "
#~ "gjøre om tastetrykk til terminaltegn."

#~ msgid "Color Scheme Error"
#~ msgstr "Feil med fargeoppsett"

#, qt-format
#~ msgid "Cannot load color scheme: %1"
#~ msgstr "Klarte ikke å laste inn fargeoppsett: %1"

#~ msgid "Color Scheme"
#~ msgstr "Fargeoppsett"

#~ msgid "FNS"
#~ msgstr "FN"

#, fuzzy
#~ msgid "SCR"
#~ msgstr "Rull"

#, fuzzy
#~ msgid "CMD"
#~ msgstr "Cmd"

#~ msgid "ALT"
#~ msgstr "ALT"

#~ msgid "SHIFT"
#~ msgstr "SHIFT"

#~ msgid "ESC"
#~ msgstr "ESC"

#~ msgid "PG_UP"
#~ msgstr "Pg_Up"

#~ msgid "PG_DN"
#~ msgstr "PG_DN"

#~ msgid "DEL"
#~ msgstr "DEL"

#, fuzzy
#~ msgid "HOME"
#~ msgstr "Home"

#~ msgid "END"
#~ msgstr "END"

#~ msgid "TAB"
#~ msgstr "TAB"

#~ msgid "ENTER"
#~ msgstr "ENTER"

#~ msgid "Login"
#~ msgstr "Logg inn"

#~ msgid "Require login"
#~ msgstr "Krev innlogging"

#~ msgid "Enable or disable login at start"
#~ msgstr "Skru på eller innlogging ved oppstart"

#~ msgid "Ok"
#~ msgstr "OK"

#~ msgid "Settings"
#~ msgstr "Innstillinger"

#~ msgid "Required"
#~ msgstr "Påkrevd"

#~ msgid "Not required"
#~ msgstr "Ikke påkrevd"

#~ msgid "Show Keyboard Bar"
#~ msgstr "Vis tastaturlinje"

#~ msgid "password"
#~ msgstr "passord"

#~ msgid "Enter password:"
#~ msgstr "Skriv inn passord:"
