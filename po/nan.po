# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-04 12:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: nan\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: lomiri-terminal-app.desktop.in:7
msgid "/usr/share/lomiri-terminal-app/terminal-app.svg"
msgstr ""

#: lomiri-terminal-app.desktop.in:8 app/qml/TerminalPage.qml:72
msgid "Terminal"
msgstr ""

#: app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr ""

#: app/qml/AlternateActionPopover.qml:73
#: app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr ""

#: app/qml/AlternateActionPopover.qml:79
#: app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr ""

#: app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr ""

#: app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr ""

#: app/qml/AlternateActionPopover.qml:98
msgid "Close active view"
msgstr ""

#: app/qml/AlternateActionPopover.qml:105
#: app/qml/Settings/SettingsShortcutsSection.qml:151 app/qml/TabsPage.qml:32
msgid "New tab"
msgstr ""

#: app/qml/AlternateActionPopover.qml:110
msgid "Close active tab"
msgstr ""

#: app/qml/AlternateActionPopover.qml:120
#: app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr ""

#: app/qml/AlternateActionPopover.qml:127 app/qml/AuthenticationDialog.qml:70
#: app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr ""

#: app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr ""

#: app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr ""

#: app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr ""

#: app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr ""

#: app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr ""

#: app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr ""

#: app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""

#: app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr ""

#: app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr ""

#. TRANSLATORS: This is the name of the Control key.
#: app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr ""

#. TRANSLATORS: This is the name of the Alt key.
#: app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr ""

#. TRANSLATORS: This is the name of the Shift key.
#: app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr ""

#. TRANSLATORS: This is the name of the Escape key.
#: app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr ""

#. TRANSLATORS: This is the name of the Page Up key.
#: app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr ""

#. TRANSLATORS: This is the name of the Page Down key.
#: app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr ""

#. TRANSLATORS: This is the name of the Delete key.
#: app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr ""

#. TRANSLATORS: This is the name of the Home key.
#: app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr ""

#. TRANSLATORS: This is the name of the End key.
#: app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr ""

#. TRANSLATORS: This is the name of the Tab key.
#: app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr ""

#. TRANSLATORS: This is the name of the Enter key.
#: app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr ""

#: app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr ""

#: app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr ""

#: app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr ""

#: app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr ""

#: app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:145
#: app/qml/Settings/SettingsShortcutsSection.qml:150
#: app/qml/Settings/SettingsShortcutsSection.qml:155
#: app/qml/Settings/SettingsShortcutsSection.qml:160
#: app/qml/Settings/SettingsShortcutsSection.qml:165
#: app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:175
#: app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:185
#: app/qml/Settings/SettingsShortcutsSection.qml:190
#: app/qml/Settings/SettingsShortcutsSection.qml:195
#: app/qml/Settings/SettingsShortcutsSection.qml:200
#: app/qml/Settings/SettingsShortcutsSection.qml:205
#: app/qml/Settings/SettingsShortcutsSection.qml:210
#: app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr ""

#: app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr ""

#: app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr ""

#: app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr ""

#: app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr ""

#: app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr ""

#: app/qml/TerminalPage.qml:326
msgid "exit selection mode"
msgstr ""
